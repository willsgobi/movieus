import { HomePage } from './../pages/home/home';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CadastrarPage } from '../pages/cadastrar/cadastrar';
import { ProgramacaoFilmesPage } from '../pages/programacao-filmes/programacao-filmes';
import { EncontrarPessoasPage } from '../pages/encontrar-pessoas/encontrar-pessoas';
import { PerfilPage } from '../pages/perfil/perfil';
import { ChatPage } from '../pages/chat/chat';
import { EsqueciSenhaPage } from '../pages/esqueci-senha/esqueci-senha';
import { ListapessoasPage } from '../pages/listapessoas/listapessoas';
import { ClassificarPage } from '..//pages/classificar/classificar';

import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { ListaPage } from '../pages/lista-filmes-adm/lista-filmes-adm';
import { ConversasPage } from '../pages/conversas/conversas';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  
  @ViewChild('myNav') NavController: ViewChild;
  rootPage: any = HomePage;


  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, afAuth: AngularFireAuth, public db: AngularFirestore) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();

      afAuth.auth.onAuthStateChanged((user) => {

        if (user === null) {
          //está logado
          splashScreen.hide();
          this.rootPage = HomePage;
          
        }
        else {
          let sub = this.db.collection('usuarios').doc<any>(user.uid).valueChanges().subscribe((user) => {
            if (user.tipo == 'usuario') {
              sub.unsubscribe();
              this.rootPage = ProgramacaoFilmesPage;
              splashScreen.hide();
            }

            else {
              sub.unsubscribe();
              this.rootPage = ListaPage;
              splashScreen.hide();
            }
          });
        }

      });

    }

    )
  }
}