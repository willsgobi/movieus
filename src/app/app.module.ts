import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CadastrarPage } from '../pages/cadastrar/cadastrar';
import { ProgramacaoFilmesPage } from '../pages/programacao-filmes/programacao-filmes';
import { EncontrarPessoasPage } from '../pages/encontrar-pessoas/encontrar-pessoas';
import { PerfilPage } from '../pages/perfil/perfil';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ChatPage } from '../pages/chat/chat';
import { EsqueciSenhaPage } from '../pages/esqueci-senha/esqueci-senha';
import { ListapessoasPage } from '../pages/listapessoas/listapessoas';
import { ClassificarPage } from '../pages/classificar/classificar';
import { CadastroPage } from '../pages/cadastro-filmes-adm/cadastro-filmes-adm';
import { ListaPage } from '../pages/lista-filmes-adm/lista-filmes-adm';
import { ConversasPage } from '../pages/conversas/conversas';
import { ListaUsuariosAdmPage } from '../pages/lista-usuarios-adm/lista-usuarios-adm';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule } from '@angular/forms';
import { AngularFireAuthModule } from '@angular/fire/auth';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyDRNQXjAMl8YEdsmMi-he1yA2MzuCGSgUo",
  authDomain: "projeto-movieus.firebaseapp.com",
  databaseURL: "https://projeto-movieus.firebaseio.com",
  projectId: "projeto-movieus",
  storageBucket: "projeto-movieus.appspot.com",
  messagingSenderId: "721101691084"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CadastrarPage,
    ProgramacaoFilmesPage,
    EncontrarPessoasPage,
    PerfilPage,
    ChatPage,
    EsqueciSenhaPage,
    ListapessoasPage,
    ClassificarPage,
    CadastroPage,
    ListaPage,
    ConversasPage,
    ListaUsuariosAdmPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CadastrarPage,
    ProgramacaoFilmesPage,
    EncontrarPessoasPage,
    PerfilPage,
    ChatPage,
    EsqueciSenhaPage,
    ListapessoasPage,
    ClassificarPage,
    CadastroPage,
    ListaPage,
    ConversasPage,
    ListaUsuariosAdmPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Camera
  ]
})
export class AppModule { }
