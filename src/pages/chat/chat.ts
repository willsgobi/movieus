import { ListaPage } from './../lista-filmes-adm/lista-filmes-adm';
import { AngularFirestore } from '@angular/fire/firestore';
import { Component, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ClassificarPage } from '../classificar/classificar';
import { Observable } from 'rxjs';
import * as firebase from 'Firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgForm } from '@angular/forms';
import { ViewChild, AfterViewChecked } from '@angular/core';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  public chat: Observable<any[]>;
  public id;
  public currentUserId;
  public idB;
  public uid;
  public lista: Observable<any[]>;
  public listaPessoa: Observable<any[]>;
  public f = {};
  public dataReal;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFirestore,
    public afAuth: AngularFireAuth) {
    this.currentUserId = afAuth.auth.currentUser.uid;
    this.id = navParams.get("id");
    // this.chat = db.collection<any>("usuarios", ref => ref.where("id", "==", this.id)).valueChanges();
    this.lista = db.collection('chat', ref => ref.orderBy('tempo')).valueChanges();
    db.collection('usuarios').doc(this.id).valueChanges().subscribe((user) => {
      this.f = user;
    });

    this.dataReal = firebase.firestore.FieldValue.serverTimestamp();
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    // method used to enable scrolling
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
  }


  public classificar(id: string): void {
    // alert(this.id)
    this.navCtrl.push(ClassificarPage, { id: this.id });
  }

  public enviar(form: NgForm): void {
    let tempo = new Date().getTime();
    let dia = new Date().getDate();
    let mes = new Date().getMonth();
    let minutos = new Date().getMinutes();
    let horas = new Date().getHours();
    let data = horas + ':' + minutos + ' - ' + dia + '/' + mes;
    let mensagem = form.value.mensagem;

    let conversa = {
      data: data,
      mensagem: mensagem,
      idA: this.currentUserId,
      idB: this.id,
      tempo: this.dataReal
    }

    this.db.collection('chat').add(conversa)
      .then((ref) => {
        let id = ref.id;
        this.db.collection('chat').doc(id).update({ id: id });
      })

    form.reset();
  }



}
