import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { PerfilPage } from '../perfil/perfil';
import { ChatPage } from '../chat/chat';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { ConversasPage } from '../conversas/conversas';
import firebase from 'firebase';

@Component({
  selector: 'page-encontrar-pessoas',
  templateUrl: 'encontrar-pessoas.html',
})
export class EncontrarPessoasPage {

  public pessoa: Observable<any[]>;
  public lista: Observable<any[]>;
  public id;
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public afAuth: AngularFireAuth, public db: AngularFirestore) {
    
    this.id = navParams.get("id")
    this.pessoa = db.collection<any>("usuarios", ref => ref.where("id", "==", this.id)).valueChanges();
  }


  public perfil(): void {
    this.navCtrl.push(PerfilPage)
  }

  public sair(): void {
    this.afAuth.auth.signOut();
  }

  public enviarMensagem(id: string): void {
    // alert(this.id)
    this.navCtrl.push(ChatPage, {id: this.id});
  }

  minhasMensagens() {
    
    this.navCtrl.push(ConversasPage);
  };

}
