import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { HomePage } from '../home/home';
import { EncontrarPessoasPage } from '../encontrar-pessoas/encontrar-pessoas';
import { PerfilPage } from '../perfil/perfil';
import { ListapessoasPage } from '../listapessoas/listapessoas';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ConversasPage } from '../conversas/conversas';

@Component({
  selector: 'page-programacao-filmes',
  templateUrl: 'programacao-filmes.html',
})
export class ProgramacaoFilmesPage {

  public lista: Observable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public afAuth: AngularFireAuth, public db: AngularFirestore) {

    this.lista = db.collection("filmes").valueChanges();
    
  }

  ngOnInit() {
  }

  public encontrar(): void {
    this.navCtrl.push(ListapessoasPage);
  }

  descricao(descricao) {
    let alert = this.alertCtrl.create({
      title: 'Descrição:',
      message: descricao,
      buttons: ['OK']
    });
    alert.present();
  }

  public sair(): void {
    this.afAuth.auth.signOut();
  }

  public perfil(): void {
    this.navCtrl.push(PerfilPage);
  }

  minhasMensagens() {
    this.navCtrl.push(ConversasPage);
  }

}
