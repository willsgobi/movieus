import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm, Form } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';


@Component({
  templateUrl: 'cadastro-filmes-adm.html',
})
export class CadastroPage {

  public imagem: string;

  constructor(
    public navCtrl: NavController,
    public db: AngularFirestore,
    public toastCtrl: ToastController,
    private camera: Camera) {
  }

  public abrirGaleria(): void {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 400,
      targetWidth: 600,
      allowEdit: true,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagem = base64Image;
    }, (err) => {
      // Handle error
    })
  }

  public salvar(form: NgForm): void {
    let nome = form.value.nome;
    let genero = form.value.genero;
    let descricao = form.value.descricao;

    let filmes = {
      nome: nome,
      genero: genero,
      descricao: descricao,
      foto: this.imagem
    };

    this.db.collection('filmes').add(filmes)
      .then((ref) => {
        let id = ref.id;
        this.db.collection('filmes').doc(id).update({ id: id });
      })
    let toast = this.toastCtrl.create({
      message: 'Cadastrado com sucesso!',
      duration: 3000
    });
    toast.present();
    this.navCtrl.pop();
  }

  public tirarFoto(): void {

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetWidth: 500
    }

    this.camera.getPicture(options).then((imagem) => {
      this.imagem = 'data:image/jpeg;base64,' + imagem;
    })

  }


}

