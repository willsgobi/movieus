import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ListaPage } from '../lista-filmes-adm/lista-filmes-adm';

@Component({
  selector: 'page-lista-usuarios-adm',
  templateUrl: 'lista-usuarios-adm.html',
})
export class ListaUsuariosAdmPage {

  public lista: Observable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFirestore) {
    this.lista = db.collection("usuarios").valueChanges();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListaUsuariosAdmPage');
  }

  public filmes(): void{
    this.navCtrl.push(ListaPage);
  }

}
