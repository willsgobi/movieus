import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  templateUrl: 'classificar.html',
})

export class ClassificarPage {

  public pessoa: Observable<any[]>;
  public lista: Observable<any[]>;
  public contadorPositivo;
  public id;
  public contadorNegativo;



  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public db: AngularFirestore) {

    this.id = navParams.get("id")
    this.pessoa = db.collection<any>("usuarios", ref => ref.where("id", "==", this.id)).valueChanges();

    // this.contadorPositivo = db.collection<any>("usuarios", ref => ref.where("contadorPositivo", "==", this.id)).valueChanges();
    // this.contadorNegativo = db.collection<any>("usuarios").valueChanges();
    // console.log(this.contadorNegativo.value);
  }


  votoNegativo(votos: number) {
    this.db.collection('usuarios/').doc(this.id).update({ contadorNegativo: (votos + 1) });
    const alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Obrigado pela classificação!',
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }
  votoPositivo(votos: number) {
    this.db.collection('usuarios').doc(this.id).update({ contadorPositivo: (votos + 1) });
    const alert = this.alertCtrl.create({
      title: '',
      subTitle: 'Obrigado pela classificação!',
      buttons: [
        {
          text: 'OK',
          handler: data => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

}
