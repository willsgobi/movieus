import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ProgramacaoFilmesPage } from '../programacao-filmes/programacao-filmes';
import { CadastrarPage } from '../cadastrar/cadastrar';
import { EsqueciSenhaPage } from '../esqueci-senha/esqueci-senha';
// import { EncontrarPessoasPage } from '../encontrar-pessoas/encontrar-pessoas';


import { NgForm } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private navCtrl: NavController, public alertCtrl: AlertController, public afAuth: AngularFireAuth) { }

  public entrar(form: NgForm) {

    let email = form.value.email;
    let senha = form.value.senha;


    this.afAuth.auth.signInWithEmailAndPassword(email, senha)
    .then((result) => {
    }) 
    .catch((error) => {
      const alert = this.alertCtrl.create({
        title: 'Ops!',
        subTitle: 'E-mail ou senha inválidos!',
        buttons: ['Tente novamente']
      });
      alert.present();
    })

  }

  public esqueciSenha(): void {
    this.navCtrl.push(EsqueciSenhaPage);
  }

  // public entrar(): void {
  //   this.navCtrl.push(ProgramacaoFilmesPage)
  // }

  public cadastrar(): void {
    this.navCtrl.push(CadastrarPage)
  }
}
