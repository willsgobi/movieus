
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { EncontrarPessoasPage } from '../encontrar-pessoas/encontrar-pessoas';
import { AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import firebase, { User } from 'firebase/app';
import { ConversasPage } from '../conversas/conversas';


@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  public f = {};
  public imagem: string;
  public imagemNova: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public afAuth: AngularFireAuth, public db: AngularFirestore, public toastCtrl: ToastController, private camera: Camera) {

    let uid = afAuth.auth.currentUser.uid;

    this.imagemNova = null;

    db.collection('usuarios').doc(uid).valueChanges().subscribe((user) => {
      this.f = user;
    });

  }

  public abrirCamera(): void {
    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 400,
      targetWidth: 400,
      allowEdit: true,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagemNova = base64Image;
    }, (err) => {
      // Handle error
    })
  }

  public abrirGaleria(): void {
    const options: CameraOptions = {
      quality: 80,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 400,
      targetWidth: 400,
      allowEdit: true,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagemNova = base64Image;
    }, (err) => {
      // Handle error
    })
  }

  public sair(): void {
    this.afAuth.auth.signOut();
  }

  public encontrar(): void {
    this.navCtrl.push(EncontrarPessoasPage);
  }

  public editar(form: NgForm): void {

    let id = form.value.id;
    let nome = form.value.nome;
    let sobrenome = form.value.sobrenome;
    let genero = form.value.genero;
    let data = form.value.data;
    let cidade = form.value.cidade;
    let descricao = form.value.descricao;


    let acao = form.value.acao;
    let animacao = form.value.animacao;
    let aventura = form.value.aventura;
    let biografia = form.value.biografia;
    let comedia = form.value.comedia;
    let cromantica = form.value.cromantica;
    let drama = form.value.drama;
    let fantasia = form.value.fantasia;
    let ficcao = form.value.ficcao;
    let romance = form.value.romance;
    let suspense = form.value.suspense;
    let terror = form.value.terror;

    if (this.imagemNova == null) {
      this.db.collection('usuarios').doc<any>(id).update({
        id: id,
        nome: nome,
        sobrenome: sobrenome,
        genero: genero,
        data: data,
        cidade: cidade,
        descricao: descricao,
        acao: acao,
        animacao: animacao,
        aventura: aventura,
        biografia: biografia,
        comedia: comedia,
        cromantica: cromantica,
        drama: drama,
        fantasia: fantasia,
        ficcao: ficcao,
        romance: romance,
        suspense: suspense,
        terror: terror
      })

      this.navCtrl.pop();
      let toastEditar = this.toastCtrl.create({
        message: 'Alterado com sucesso!',
        duration: 2000,
        // position: 'middle'
      });
      toastEditar.present();


    } else {
      this.db.collection('usuarios').doc<any>(id).update({
        id: id,
        nome: nome,
        sobrenome: sobrenome,
        genero: genero,
        data: data,
        cidade: cidade,
        descricao: descricao,
        acao: acao,
        animacao: animacao,
        aventura: aventura,
        biografia: biografia,
        comedia: comedia,
        cromantica: cromantica,
        drama: drama,
        fantasia: fantasia,
        ficcao: ficcao,
        romance: romance,
        suspense: suspense,
        terror: terror,
        foto: this.imagemNova
      })

      this.navCtrl.pop();
      let toastEditar = this.toastCtrl.create({
        message: 'Alterado com sucesso!',
        duration: 2000,
        // position: 'middle'
      });
      toastEditar.present();

    }

    // this.db.collection('usuarios').doc<any>(id).update({
    //   id: id,
    //   nome: nome,
    //   sobrenome: sobrenome,
    //   genero: genero,
    //   data: data,
    //   cidade: cidade,
    //   descricao: descricao,
    //   acao: acao,
    //   animacao: animacao,
    //   aventura: aventura,
    //   biografia: biografia,
    //   comedia: comedia,
    //   cromantica: cromantica,
    //   drama: drama,
    //   fantasia: fantasia,
    //   ficcao: ficcao,
    //   romance: romance,
    //   suspense: suspense,
    //   terror: terror,
    //   foto: this.imagem


  }

  minhasMensagens() {
    this.navCtrl.push(ConversasPage)
  }

  // excluirConta(id: string): void {
  //   this.db.collection('usuarios').doc(id).delete().then(() => {
  //     this.afAuth.auth.signOut();
  //   })

  // }

  // imagem(): void{
  //   const alert = this.alertCtrl.create({
  //     title: '',
  //     subTitle: 'Selecione a imagem!',
  //     buttons: ['OK']
  //   });
  //   alert.present();
  // }
}