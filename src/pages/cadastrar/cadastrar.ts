import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProgramacaoFilmesPage } from '../programacao-filmes/programacao-filmes';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { templateJitUrl } from '@angular/compiler';

/**
 * Generated class for the CadastrarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-cadastrar',
  templateUrl: 'cadastrar.html',
})
export class CadastrarPage {

  public imagem: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFirestore, public afAuth: AngularFireAuth, private camera: Camera) {
  }

  public abrirCamera(): void {
    const options: CameraOptions = {
      quality: 80,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 400,
      targetWidth: 400,
      allowEdit: true,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagem = base64Image;
    }, (err) => {
      // Handle error
    })
  }

  public abrirGaleria(): void {
    const options: CameraOptions = {
      quality: 80,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 400,
      targetWidth: 400,
      allowEdit: true,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagem = base64Image;
    }, (err) => {
      // Handle error
    })
  }

  public cadastrar(form: NgForm): void {

    let email = form.value.email;
    let senha = form.value.senha;

    this.afAuth.auth.createUserWithEmailAndPassword(email, senha).then((result) => {

      let id = result.user.uid;

      let nome = form.value.nome;
      let sobrenome = form.value.sobrenome;
      let genero = form.value.genero;
      let data = form.value.data;
      let cidade = form.value.cidade;
      let descricao = form.value.descricao;
      // let imagem = form.value.imagem;
      // console.log(this.imagem);

      let acao = form.value.acao;
      if (form.value.acao == true) {
        acao = true;
      } else {
        acao = false
      }

      let animacao = form.value.animacao;
      if (animacao == true) {
        animacao = true;
      } else {
        animacao = false;
      }

      let aventura = form.value.aventura;
      if (aventura == true) {
        aventura = true;
      } else {
        aventura = false;
      }

      let biografia = form.value.biografia;
      if (biografia == true) {
        biografia = true;
      } else {
        biografia = false;
      }

      let comedia = form.value.comedia;
      if (comedia == true) {
        comedia = true;
      } else {
        comedia = false;
      }

      let cromantica = form.value.cromantica;
      if (cromantica == true) {
        cromantica = true;
      } else {
        cromantica = false;
      }

      let drama = form.value.drama;
      if (drama == true) {
        drama = true;
      } else {
        drama = false;
      }

      let fantasia = form.value.fantasia;
      if (fantasia == true) {
        fantasia = true;
      } else {
        fantasia = false;
      }

      let ficcao = form.value.ficcao;
      if (ficcao == true) {
        ficcao = true;
      } else {
        ficcao = false;
      }

      let romance = form.value.romance;
      if (romance == true) {
        romance = true;
      } else {
        romance = false;
      }

      let suspense = form.value.suspense;
      if (suspense == true) {
        suspense = true;
      } else {
        suspense = false;
      }

      let terror = form.value.terror;
      if (terror == true) {
        terror = true;
      } else {
        terror = false;
      }

      let usuario = {
        id: id,
        nome: nome,
        sobrenome: sobrenome,
        genero: genero,
        data: data,
        cidade: cidade,
        descricao: descricao,
        email: email,
        acao: acao,
        animacao: animacao,
        aventura: aventura,
        biografia: biografia,
        comedia: comedia,
        cromantica: cromantica,
        drama: drama,
        fantasia: fantasia,
        ficcao: ficcao,
        romance: romance,
        suspense: suspense,
        terror: terror,
        contadorPositivo: 0,
        contadorNegativo: 0,
        tipo: "usuario",
        foto: this.imagem
      }

      this.db.collection('usuarios').doc(id).set(usuario)
        .then(() => {
          this.navCtrl.push(ProgramacaoFilmesPage);
        })

    }).catch((error) => {
      alert(error);
    })

  };


  public encontrar(): void {
    this.navCtrl.push(ProgramacaoFilmesPage);
  }
}
