import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ListapessoasPage } from '../listapessoas/listapessoas';
import { PerfilPage } from '../perfil/perfil';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'page-conversas',
  templateUrl: 'conversas.html',
})
export class ConversasPage {

  public lista: Observable<any[]>;
  public usuario: Observable<any[]>;
  public id;
  public currentUserId;
  public combinedList;

  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public db: AngularFirestore) {
    this.currentUserId = afAuth.auth.currentUser.uid;
    this.id = navParams.get("id");
    this.usuario = db.collection("usuarios").valueChanges();
    this.lista = db.collection("chat", ref => ref.orderBy('tempo')).valueChanges();
    this.combinedList = combineLatest<any[]>(this.lista, this.usuario).pipe(
      map(arr => arr.reduce((acc, cur) => acc.concat(cur))),
    )
  }


  public encontrar(): void {
    this.navCtrl.push(ListapessoasPage);
  }

  public perfil(): void {
    this.navCtrl.push(PerfilPage);
  }

  public sair(): void {
    this.afAuth.auth.signOut();
  }

  public verConversa(): void {
    this.navCtrl.push(ConversasPage, { id: this.id });
  }
}
