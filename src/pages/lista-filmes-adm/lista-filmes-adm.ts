import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Camera, CameraOptions } from '@ionic-native/camera';

import { CadastroPage } from '../cadastro-filmes-adm/cadastro-filmes-adm';
import { ListaUsuariosAdmPage } from '../lista-usuarios-adm/lista-usuarios-adm';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgForm, Form } from '@angular/forms';

@Component({
  templateUrl: 'lista-filmes-adm.html',
})
export class ListaPage {

  public lista: Observable<any[]>
  private id;

  public nome;
  public genero;
  public descricaoFilme;
  public imagem: string;
  public imagemNova: string;


  constructor(public navCtrl: NavController, public db: AngularFirestore, public afAuth: AngularFireAuth, public alertCtrl: AlertController, public toastCtrl: ToastController, private camera: Camera) {

    this.lista = db.collection('filmes').valueChanges();
  }

  public abrirGaleria(): void {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetHeight: 500,
      targetWidth: 500,
      allowEdit: true,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.imagemNova = base64Image;
    }, (err) => {
      // Handle error
    })
  }

  public apagar(id: string): void {
    this.db.collection('filmes').doc(id).delete();
    let toast = this.toastCtrl.create({
      message: 'Apagado com sucesso!',
      duration: 3000
    });
    toast.present();
  }

  public cadastrar(): void {
    this.navCtrl.push(CadastroPage);

  }

  public editar(form: NgForm): void {

    let id = form.value.id;
    let nome = form.value.nome;
    let genero = form.value.genero;
    let descricao = form.value.descricao;

    this.db.collection('filmes').doc<any>(id).update({
      id,
      nome: nome,
      genero: genero,
      descricao: descricao
      // foto: this.imagemNova
    });
    this.imagemNova = null;
    let toastEditar = this.toastCtrl.create({
      message: 'Alterado com sucesso!',
      duration: 3000
    });
    toastEditar.present();

  }

  public sair(): void {
    this.afAuth.auth.signOut();
  }

  public usuarios(): void{
    this.navCtrl.push(ListaUsuariosAdmPage);
  }

}
