import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoadingController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-esqueci-senha',
  templateUrl: 'esqueci-senha.html',
})
export class EsqueciSenhaPage {
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public afAuth: AngularFireAuth, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EsqueciSenhaPage');
  }

  enviarLembrete() {
    
  }

  public recuperar(form: NgForm){
    let email = form.value.email;

    this.afAuth.auth.sendPasswordResetEmail(email)
    .then(() => {
      let toastEditar = this.toastCtrl.create({
        message: 'Lembrete enviado! Verifique seu e-mail.',
        duration: 2000,
        position: 'center'
      });
      toastEditar.present();
      this.navCtrl.pop();
    })
    .catch((error) => {
      alert(error);
    })
  }
}
