import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { EncontrarPessoasPage } from '../encontrar-pessoas/encontrar-pessoas';
import { PerfilPage } from '../perfil/perfil';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { ConversasPage } from '../conversas/conversas';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'page-listapessoas',
  templateUrl: 'listapessoas.html',
})
export class ListapessoasPage {

  public lista: Observable<any[]>;
  public uIdA = {};
  public currentUserId;


  constructor(public navCtrl: NavController, public navParams: NavParams, public afAuth: AngularFireAuth, public alertCtrl: AlertController, public db: AngularFirestore) {

    this.currentUserId = afAuth.auth.currentUser.uid;

    this.uIdA = navParams.get('id');
    this.lista = db.collection("usuarios").valueChanges();
  }

  public sair(): void {
    this.navCtrl.push(HomePage);
  }

  public perfil(): void {
    this.navCtrl.push(PerfilPage);
  }

  minhasMensagens() {

    this.navCtrl.push(ConversasPage);
  }

  public verPessoa(id: string): void {
    // alert(this.uIdA); // alerta 'seuid'
    this.navCtrl.push(EncontrarPessoasPage, { id: id });
  }

}
